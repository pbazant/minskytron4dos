org 100h
mov al,13h
int 10h
push word 0A000h
pop	es

;3c8h index
;3c9h rgb

mov dx,3c8h
xor al,al
out dx,al
inc dx
P:
out dx,al
out dx,al
out dx,al
out dx,al
out dx,al
out dx,al
out dx,al
out dx,al
out dx,al
out dx,al
out dx,al
out dx,al
inc ax
jnz P

L:

xor di,di
mov bp, 0
mov cx, 0
M:

mov ax,bp
;shr ax,2
mul ax
mov si,cx
;shr si,2
mul si
mul si

shr ax,1

;imul bx,3
;mov ah,0
;add bx,ax
;shr bx,2

;shl al,1

cmp byte [flag], 0
jnz noblur

xor si,si
xor ax,ax

mov al,[es:di]

imul ax,60

add si,ax

xor ax,ax

mov al,[es:di+1]
add si,ax
mov al,[es:di+320]
add si,ax
mov al,[es:di-1]
add si,ax
mov al,[es:di-320]
add si,ax


;shr si,4
;inc si
;shr si,1

xor ax,ax
cmp si,128*64
salc ; set al to carry
and ax,120
sub si,ax
add si,60
add si,32
add si,28 ; tweak, but why?
shr si,6

xchg si,ax

cmp ax,255

jna oflw
mov al,255
oflw:

noblur:

mov [es:di],al

inc di

inc bp
cmp bp,320
jnz M

xor bp,bp
inc cx
cmp cx,200
jnz M

;dec byte [flag]
mov byte [flag],0

in al,60h
dec al
jnz L

ret

flag: db 255