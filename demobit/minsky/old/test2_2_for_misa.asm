org 100h
mov al,13h
int 10h
push word 0A000h
pop	es

L:

mov ax, word [b_x]
mov bx, word [b_y]

add ax, word [a_y]
sub bx, word [a_x]

sar ax, 11
sar bx, 10

add word [a_x], ax
add word [a_y], bx



mov ax, word [c_x]
mov bx, word [c_y]

add ax, word [b_y]
sub bx, word [b_x]

sar ax, 9
sar bx, 11

add word [b_x], ax
add word [b_y], bx



mov ax, word [a_x]
mov bx, word [a_y]

add ax, word [c_y]
sub bx, word [c_x]

sar ax, 6
sar bx, 5

add word [c_x], ax
add word [c_y], bx



mov ax, word [a_x]
mov bx, word [a_y]

sar ax,8
sar bx,8

add bx,128
imul bx,320
add bx,ax
add bx,160

mov di,bx
mov [es:di], word 2+512
mov [es:di+320], word 2+512



mov ax, word [b_x]
mov bx, word [b_y]

sar ax,8
sar bx,8

add bx,128
imul bx,320
add bx,ax
add bx,160

mov di,bx
mov [es:di], word 3+512+256
mov [es:di+320], word 3+512+256



mov ax, word [c_x]
mov bx, word [c_y]

sar ax,8
sar bx,8

add bx,128
imul bx,320
add bx,ax
add bx,160

mov di,bx
mov [es:di], word 4+1024
mov [es:di+320], word 4+1024


push cx

mov cx,200
G:
add si, 40499
mov [es:si], byte 0
loop G
mov cx,1000
GG:
loop GG

pop cx

in al,60h
dec al
jnz L

ret

a_x: dw 256
a_y: dw 256

b_x: dw 0
b_y: dw 0

c_x: dw 0
c_y: dw 4096
