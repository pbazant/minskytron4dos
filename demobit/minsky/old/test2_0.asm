org 100h
mov al,13h
int 10h
push word 0A000h
pop	es

mov ax, 0908h
mov bx, 0900h
mov cx, 0009h

L:

mov dl, bl
mov dh, bh

add dl, ah
sub dh, al

shr dl, 5
shr dh, 6

add al, dl
add ah, dh


mov dl, cl
mov dh, ch

add dl, bh
sub dh, bl

shr dl, 6
shr dh, 6

add bl, dl
add bh, dh


mov dl, al
mov dh, ah

add dl, ch
sub dh, cl

shr dl, 6
shr dh, 5

add cl, dl
add ch, dh

mov si,320

push bx

mov bx, ax
shr bx, 8
add bx, 128
imul bx, si
xor dx,dx
mov dl, al
add bx, dx
add bx, 128
mov di,bx
mov [es:di], byte 2

pop bx
push cx

mov cx, bx
shr cx, 8
add cx, 128
imul cx, si
xor dx,dx
mov dl, bl
add cx, dx
add cx, 128
mov di,cx
mov [es:di], byte 2

pop cx
push ax

mov ax, cx
shr ax, 8
add ax, 128
imul ax, si
xor dx,dx
mov dl, cl
add ax, dx
add ax, 128
mov di,ax
mov [es:di], byte 2

pop ax

push cx
mov cx,10000
G:
loop G
pop cx

in al,60h
dec al
jnz L

ret

flag: db 255