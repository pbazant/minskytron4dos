; an x86 variation on Minskytron
; by Pavel Bazant pbazant@gmail.com

; I used the following sources of information:
; http://www.masswerk.at/minskytron/
; http://www.masswerk.at/minskytron/minskytron-annotated.txt

org 100h
; https://en.wikipedia.org/wiki/VESA_BIOS_Extensions
mov ax, 4f02h
mov bx, 0103h ; 800x600, 256-color
int 0x10
push word 0A000h
pop	es

L:
mov di, a
mov si, b
mov al, 2
mov cx, 0807h
call iter_add

mov di, b
mov si, c
mov al, 3
mov cx, 0808h
call iter_sub

mov di, c
mov si, a
mov al, 4
mov cx, 0203h
call iter_sub

mov cx,300
G:
push cx

add [d0], dword 2654435769
mov ax, [d0]
mov cx, ax
and cx, 511
shr ax, 15
add cx, ax
add cx, 144

mov ax, [d1]
mov dx, ax
and dx, 511
shr ax, 15
add dx, ax
add dx, 44

mov ah,0ch
mov al,0
mov bx, 0; is this necessary?
int 10h

pop cx
loop G

;mov cx, slowdown
;GG:
;loop GG

in al,60h
dec al
jnz L

ret

iter_add:
mov bx, [di]
add bx, [si]
jmp iter

iter_sub:
mov bx, [di]
sub bx, [si]

iter:
sar bx, cl

add bx, [di+2]
mov [di+2], bx

sub bx, [si+2]
xchg ch, cl; or ror 8?
sar bx, cl
neg bx

add bx,[di]
mov [di], bx

mov cx, bx
mov dx, [di+2]

sar cx,7
sar dx,7
add cx,256+144; 144 = (800-512)/2
add dx,256+44; 44 = (600-512)/2
mov ah,0ch
; al set by caller
mov bx, 0 ; is this necessary?
int 10h

inc cx
int 10h
inc dx
int 10h
dec cx
int 10h
ret


a:
dw 0
dw -7601

b:
dw -3840
dw 0

c:
dw 0
dw -1280

d0: dw 0
d1: dw 0
