org 100h
; https://en.wikipedia.org/wiki/VESA_BIOS_Extensions
mov ax, 4f02h
;mov bx, 0104h ; 1024×768, 16-color
;or bx, 4000h ; "use linear frame buffer", was tha???
mov bx, 0103h ;
int 0x10

push word 0A000h
pop	es

L:

mov di, a
mov si, b
call iter_add
mov di, si
mov si, c
call iter_sub
mov di, si
mov si, a
call iter_sub

mov cx,200
G:
push cx
add [d0], dword 2654435769
mov cx, [d0]
mov dx, [d1]
and cx, 511
and dx, 511
add cx, 144

mov ah,0ch
mov al,0
int 10h
pop cx
loop G

mov cx,1
GG:
loop GG

in al,60h
dec al
jnz L

ret



iter_add:
mov cx, [di]
add cx, [si]
jmp iter

iter_sub:
mov cx, [di]
sub cx, [si]

iter:
sar cx, 6+1

add cx, [di+1]
mov [di+1], cx

sub cx, [si+1]
sar cx, 7+1
neg cx

add cx,[di]
mov [di], cx

mov dx, [di+1]

sar cx,7
sar dx,7
add cx,400
inc dh
mov ah,0ch
mov al,2
int 10h

inc cx
int 10h
inc dx
int 10h
dec cx
int 10h
ret


a:
dw 0
dw -6081-1520

b:
dw -3072-768
dw 0

c:
dw 0
dw -1024-256

;xa: dw -12163
;xb: dw 0
;xc: dw 2048
;ya: dw 0
;yb: dw 6144
;yc: dw 0

d0: dw 0
d1: dw 0
