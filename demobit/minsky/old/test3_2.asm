org 100h
; https://en.wikipedia.org/wiki/VESA_BIOS_Extensions
mov ax, 4f02h
;mov bx, 0104h ; 1024×768, 16-color
;or bx, 4000h ; "use linear frame buffer", was tha???
mov bx, 0103h ;
int 0x10

push word 0A000h
pop	es

L:

mov cx, word [xa]
add cx, word [xb]
sar cx, 6+1

add cx, [ya]
mov [ya], cx

sub cx, [yb]
sar cx, 7+1
neg cx

add cx,[xa]
mov [xa], cx

mov dx, [ya]

sar cx,7
sar dx,7
inc ch
inc dh
mov ah,0ch
mov al,2
int 10h



mov cx, word [xb]
sub cx, word [xc]
sar cx, 7+1

add cx, [yb]
mov [yb], cx

sub cx, [yc]
sar cx, 7+1
neg cx

add cx,[xb]
mov [xb], cx

mov dx, [yb]

sar cx,7
sar dx,7
inc ch
inc dh
mov ah,0ch
mov al,2
int 10h



mov cx, word [xc]
sub cx, word [xa]
sar cx, 2+1

add cx, [yc]
mov [yc], cx

sub cx, [ya]
sar cx, 1+1
neg cx

add cx,[xc]
mov [xc], cx

mov dx, [yc]

sar cx,7
sar dx,7
inc ch
inc dh
mov ah,0ch
mov al,2
int 10h


mov cx,200
G:
push cx
add [d0], dword 2654435769
mov cx, [d0]
mov dx, [d1]
and cx, 511
and dx, 511

mov ah,0ch
mov al,0
int 10h
pop cx
loop G

mov cx,1
GG:
loop GG

in al,60h
dec al
jnz L

ret


ya: dw -6081-1520
yb: dw 0
yc: dw -1024-256
xa: dw 0
xb: dw -3072-768
xc: dw 0


;ya: dw 3040
;yb: dw 0
;yc: dw 512
;xa: dw 0
;xb: dw 1536
;xc: dw 0

;xa: dw -12163
;xb: dw 0
;xc: dw 2048
;ya: dw 0
;yb: dw 6144
;yc: dw 0

d0: dw 0
d1: dw 0
