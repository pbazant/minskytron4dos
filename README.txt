This is a variation on the Minskytron -- a graphical demo for the PDP-1 created by Marvin Minsky.

To run the demo, run minsky.com in DOS or a DOS emulator (tested in DosBox).
You can edit minsky.asm and run cc.com to compile and run the edited version.

A JavaScript emulation of PDP-1 running the original Minskytron and other programs can be found here:
http://www.masswerk.at/minskytron/
